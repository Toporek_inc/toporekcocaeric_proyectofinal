Toporek Coca Eric
31428498-7

=============================
* La única estructura de datos que se maneja son los arreglos, por la facilidad de navegación y operabilidad que estos ofrecen.

==================================
* El proyecto ha sido desarrollado en NetBeans IDE version 8.2

==================================
* El ingreso de la contraseña tiene que ser estrictamente numérico, de lo contrario se arroja excepción de Scanner
* No puedo determinar que la lectura/escritura de un archivo sea posible en documentos de extensión diferente a '.txt'

========== Clases: =========== 
* Main : contiene el método principal así como la muestra de los Menús
* Usuario (abstracta): Contiene las variables de un respectivo usuario, así como una sobreescritura al métdo equals.
* Admin: Hereda de Usuario, que modifica una variable para acceder a permisos de administrador
* User: Hereda de Usuario, realmente no hace diferencia alguna.
* BaseDeDatosUsuario: Clase que permite la gestión de los usuarios registrados en el archivo login.txt
* LecturaEscritura: Clase que facilita la lectura y escritura de archivos.
* BuscadorDeCadenas: Contiene el algoritmo para búsqueda de ocurrencias en cadenas de texto
* UserException: Excepción de usuario, arrojada cuando no hay coincidencias en el Log-in.
* SearchException: Excepción en la búsqueda, arrojada cuando no hay ocurrencias de un patrón en el texto.