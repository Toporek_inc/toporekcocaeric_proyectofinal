package icc.main;

import icc.util.*;
import icc.users.*;
import java.io.IOException;
import java.util.Scanner;

/**
 * 
 * Proyecto Final
 * ICC 2019-1
 * Prof. Virgina Teodosio
 * Ayudantes: Isaac Pompa, Leonardo Gallo
 * Clase principal
 * @title Main
 * @author Eric Toporek
 * @version 2.0
 * @date 12/12/18
 * 
 */
public class Main { 

    /**
     * Despliegue del menú para un usuario administrador
     */
    public static void getMenuA() {
        System.out.println("Usuario Administrador, dispone de las siguientes opciones de salida: " 
                           + "\n1.Salida estandar (consola)." 
                           + "\n2.Salida a archivo de texto.");
    }
    
    /**
     * Despliegue del menú estandar.
     */
    public static void getMenu(){
        System.out.println("Es momento de buscar un patrón de texto."
                          + "\nIngrese una de las siguientes opciones:"
                          + "\n1.Para buscar desde un archivo." 
                          + "\n2.Para buscar desde una línea de texto.");
    }
    
    /**
     * Método principal
     * @param args 
     */
    public static void main(String[]args){
        /** Variable de ID de usuario */
        String id;
        /** La contraseña siempre consta de un entero */
        int password;
        /** Operadores para el menú */
        byte eleccion = 0; 
        byte subeleccion;
        /** Cadenas para la implementación del algoritmo */
        String src, out, textoA, textoB;
        String[] search = null;
        /**Objetos a utilizar*/
        Usuario temporal;
        LecturaEscritura io = new LecturaEscritura();
        BuscadorDeCadenas google = new BuscadorDeCadenas();
        
        /** Métodos para la lectura del Log-in.*/
        BaseDeDatosUsuario usrs = new BaseDeDatosUsuario();
        Usuario[] users = usrs.generaBase();
        
        /* Se despliega el menú de bienvenida */
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese su ID de usuario y su contraseña numérica: ");
       
        
        /* Inicio de sesión */
        for( ; ; ){ 
            id = sc.nextLine();
            password = sc.nextInt();
            temporal = new User(id,password, "");
            try{
                temporal = temporal.loginUsuario(users);
                break;
            }catch (UserException ex){
                System.err.println("Usuario no encontrado, intente nuevamente");
            }
        }
        
        if(temporal.isLogged())
            System.out.println("\nBienvenido " + temporal.getNombre());
        /** Interfaz del usuario */
        while(temporal.isLogged()){
            switch(temporal.numericAdmin()){
                case 1:
                    getMenuA();
                    eleccion = sc.nextByte();
                default:
                    for( ; ; ){
                        getMenu();
                        subeleccion = sc.nextByte();
                        sc.nextLine();
                        if(subeleccion == 1){
                            for( ; ; ){
                                try{
                                    System.out.println("Ingrese la ruta de acceso al texto: ");
                                    src = sc.nextLine();
                                    textoA = io.leerUnaLinea(src);
                                    break;
                                }catch(IOException ex){
                                    System.err.println("Ruta no encontrada, intente de nuevo.");
                                }
                            }
                            break;
                        }else if (subeleccion == 2){
                            System.out.println("Escriba el texto a ser buscado: ");
                            textoA = sc.nextLine();
                            break;
                        }else
                            System.err.println("Parametro no válido");
                    }
                    System.out.println("Ingrese el texto que desea que sea buscado: ");
                    textoB = sc.nextLine();
                    try {
                        search = google.busqueda(textoA, textoB);
                    } catch (SearchException ex) {
                        search = new String[0];
                        System.out.println("No hay coincidencias de búsqueda.");
                    }
                    String[] report = new String[search.length + 4];
                    report[0] = temporal.getId() + ":" + temporal.getNombre();
                    report[1] = "Texto: " + textoA;
                    report[2] = "Patrón de busqueda: " + textoB;
                    report[3] = "Número de ocurrencias: " + Integer.toString(search.length); 
                    int j = 0;
                    for(int i = 4; i < report.length ; i++){
                        report[i] = search[j];
                        j++;
                    }
                    if(temporal.isAdmin() && eleccion == 2){
                        out = "data/salida" + temporal.getId() + ".txt";
                        try {
                            io.escribeArchivo(report, out);
                            System.out.println("Se proporciona un archivo de reporte exitosamente. dentro de la carpeta data.");
                        } catch (IOException ex) {
                            System.out.println("Error de ruta.");
                        }
                    }else{
                        for(int k = 0; k < report.length; k++){
                            System.out.println(report[k]);
                        }
                    }
             }
            System.out.println("¿Desea cerrar sesión? Presione 0, de lo contrario, 1.");
            eleccion = sc.nextByte();
            if(eleccion == 0){
                System.out.println("\nCerrando sesión.");
                temporal.setLogged(false);
            }
        }
        sc.close();
        System.out.println("Hasta luego.");
    }    
}

