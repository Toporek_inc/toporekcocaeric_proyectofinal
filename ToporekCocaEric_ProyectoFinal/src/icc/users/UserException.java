package icc.users;

/**
 * 
 * Proyecto Final
 * ICC 2019-1
 * Prof. Virgina Teodosio
 * Ayudantes: Isaac Pompa, Leonardo Gallo
 * Excepción de un usuario
 * @title UserException
 * @author Eric Toporek
 * @version 1.0
 * @date 12/12/18
 * 
 */
public class UserException extends Exception{
    
    /** Constructores */
    public UserException() {
    }

    public UserException(String string) {
        super(string);
    }
    
    
}
