package icc.users;
import icc.util.LecturaEscritura;
import java.io.IOException;
import java.util.Arrays;


/**
 * Proyecto Final
 * ICC 2019-1
 * Prof: Virginia Teodosio
 * Clase que gestiona un grupo de usuarios
 * @author Eric Toporek
 * @version 1.0
 */
public class BaseDeDatosUsuario {
    
    private Usuario[] baseUsuario;
    
    /**
     * Método que genera un arreglo de tipo Usuario
     * A partir de la lectura del archivo log-in
     * @return Arreglo de usuarios registrados
     */
    public Usuario[] generaBase(){
        //String test = "C:/users/etopo/Documents/NetBeansProjects/ToporekCocaEric_ProyectoFinal/data/test.txt";
        String src = "data/login.txt";
        LecturaEscritura io = new LecturaEscritura();
        try {
            String[][] data = io.leerLogIn(src);
            baseUsuario = new Usuario[data.length];
            for(int i = 0; i < data.length; i++){
                if(data[i][2].equals("admin"))
                    baseUsuario[i] = new Admin(data[i][0],Integer.parseInt(data[i][1]),data[i][3]); 
                else
                    baseUsuario[i] = new User(data[i][0],Integer.parseInt(data[i][1]),data[i][3]);
            }
        } catch (IOException ex) {
            System.err.println("Archivo Log-in no encontrado");
        }
        return baseUsuario;
    }
}
