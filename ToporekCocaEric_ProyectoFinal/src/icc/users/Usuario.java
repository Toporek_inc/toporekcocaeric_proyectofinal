package icc.users;


/**
 * Proyecto Final
 * ICC 2019-1
 * Prof. Virginia Teodosio
 * Clase abstracta de un usuario
 * @author Eric Toporek
 * @version 1.2
 */
public abstract class Usuario {
    /** Nombre*/
    protected String nombre;
    /** ID de usuario*/
    protected String id;
    /** Contraseña*/
    protected int password;
    /** Variable que genera permisos de administrador y que establece un estado sobre una sesión*/
    protected boolean admin, logged;
    
    /**
     * Constructor de un usuario
     * @param id
     * @param password
     * @param nombre 
     */
    public Usuario(String id, int password, String nombre) {
        this.nombre = nombre;
        this.id = id;
        this.password = password;
    }

    /*===== GETTERS & SETTERS ==== */
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getPassword() {
        return password;
    }

    public void setPassword(int password) {
        this.password = password;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public boolean isLogged() {
        return logged;
    }

    public void setLogged(boolean logged) {
        this.logged = logged;
    }
    
    /**
     * Comparación sobre la igualdad de los objetos de tipo Usuario
     * @return Estado booleano
     */
    @Override
    public boolean equals(Object obj){
        if(obj instanceof Usuario){
            return this.id.equals(((Usuario) obj).id) && this.password ==(((Usuario) obj).password);
        }else{
            System.err.println("No se pueden comparar objetos");
            return false;
        }
    }
    
    /**
     * Método para inicio de sesión de un usuario.
     * @param users
     * @return booleano que determina un inicio de sesión
     * @throws UserException 
     */
    public boolean login (Usuario[] users) throws UserException{
        boolean match = false;
        for(int i = 0 ; i < users.length ; i ++){
            if (this.equals(users[i])){
                match = true;
                break;
            }
        }
        if(match == true){
            this.logged = true;
        }else{
            this.logged = false;
            throw new UserException("Usuario no encontrado");
        }
        
        return this.logged;
    }
    
    /**
     * Método de inicio de sesión en términos de usuario
     * @param users
     * @return Usuario de coincidencia.
     * @throws icc.users.UserException
     */
    public Usuario loginUsuario (Usuario[] users) throws UserException{
        Usuario temp = null;
        for(int i = 0; i < users.length ; i++){
            if(this.equals(users[i])){
                temp = users[i];
                break;
            }
        }
        if(temp != null){
            temp.logged = true;            
        }else
            throw new UserException ("Usuario no encontrado");
        
        return temp;      
    }
    
    /**
     * Método para establecer valor de verdad sobre la variable admin
     * @return 1 en case de ser cierto, 0 de ser falso.
     */
    public int numericAdmin(){
        if(this.admin){
            return 1;
        }else
            return 0;
    }
}
