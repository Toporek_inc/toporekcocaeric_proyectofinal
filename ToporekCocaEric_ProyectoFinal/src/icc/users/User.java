package icc.users;

/**
 * 
 * Proyecto Final
 * ICC 2019-1
 * Prof. Virgina Teodosio
 * Ayudantes: Isaac Pompa, Leonardo Gallo
 * Clase hija de tipo usuario, modifica los permisos de administrador
 * @title User
 * @author Eric Toporek
 * @version 2.0
 * @date 12/12/18
 * 
 */
public class User extends Usuario {

    public User(String id, int password, String nombre) {
        super(id, password, nombre);
        this.admin = false;
    }

    
}
