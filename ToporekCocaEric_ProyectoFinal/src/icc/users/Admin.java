package icc.users;

/**
 * 
 * Proyecto Final
 * ICC 2019-1
 * Prof. Virgina Teodosio
 * Ayudantes: Isaac Pompa, Leonardo Gallo
 * Clase hija de tipo usuario, que modifica los permisos de administrador.
 * @title Admin
 * @author Eric Toporek
 * @version 1.0
 * @date 12/12/18
 * 
 */
public class Admin extends Usuario{

    public Admin(String id, int password, String nombre) {
        super(id, password, nombre);
        this.admin = true;
    }

    
}
