package icc.util;

/**
 * 
 * Proyecto Final
 * ICC 2019-1
 * Prof. Virgina Teodosio
 * Ayudantes: Isaac Pompa, Leonardo Gallo
 * Clase de utilidad para buscador de cadenas
 * @title BuscadorDeCadenas
 * @author Eric Toporek
 * @version 2.0
 * @date 12/12/18
 * 
 */
public class BuscadorDeCadenas {
    
    /**
     * Método de búsqueda
     * @param txt una cadena grande de texto
     * @param pttrn patron de búsqueda.
     * @return Arreglo de cadenas con ocurrencias
     * @throws icc.util.SearchException 
     */
    public String[] busqueda(String txt, String pttrn) throws SearchException{
        
        //Almacenamiento de las cadenas en arreglos de texto
        String[] resultados;
        char[] texto = txt.toCharArray();
        char[] patron = pttrn.toCharArray();
        int ocurrencias = 0;
        for(int i = 0; i < texto.length ; i++){
            int j;
            for( j = 0; j < patron.length ; j++){
                if(patron[j] != texto[i + j])
                    break;
            }
            if(j == patron.length)
                ocurrencias++;
        }
        resultados = new String[ocurrencias];
        if(ocurrencias == 0){
            throw new SearchException("No hay ocurrencias del patrón dado");
        }
        int k = 0;
        for(int i = 0; i < texto.length ; i++){
            int j;
            for( j = 0; j < patron.length ; j++){
                if(patron[j] != texto[i + j])
                    break;
            }
            if(j == patron.length){
                resultados[k] = "Ocurrencia " + Integer.toString(k +1) + " en: " + Integer.toString(i);
                k++;
            }
        }
        
        return resultados;
    }
}
