package icc.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


/**
 * 
 * Proyecto Final
 * ICC 2019-1
 * Prof. Virginia Teodosio
 * Ayudantes: Isaac Pompa, Leonardo Gallo
 * Clase de utilidad para la lectura/escritura de archivos
 * @title LecturaEscritura
 * @author Eric Toporek
 * @version 2.0
 * @date 12/12/18
 * 
 */
public class LecturaEscritura {
    
    /**
     * Método de lectura de archivos.
     * @param ruta 
     * @return Arreglo de cadenas según la linea 
     * @throws java.io.IOException 
     */
    public String[] leerArchivo(String ruta) throws IOException{
        BufferedReader contador = new BufferedReader(new FileReader(ruta));
        String s = contador.readLine();
        int n = 0;
        while(s != null){
            n++;
            s = contador.readLine();
            
        }
        contador.close();
        String[] lectura = new String[n];
        BufferedReader flux = contador;
        int i = 0;
        String read = flux.readLine();
        while(read != null){
            lectura[i] = read;
            i++;
            read = flux.readLine();
        }
        flux.close();
        return lectura;
    }
    
    /**
     * 
     * @param ruta
     * @return
     * @throws IOException 
     */
    public static String[][] leerLogIn(String ruta) throws IOException{
        BufferedReader contador = new BufferedReader(new FileReader(ruta));
        String s = contador.readLine();
        int n = 0;
        while(s != null){
            n++;
            s = contador.readLine();
            
        }
        contador.close();
        String[][] data = new String[n][];
        BufferedReader flux = new BufferedReader(new FileReader(ruta));
        String read = flux.readLine();
        int i = 0;
        while(read != null){
            data[i] = read.split(":");
            i++;
            read = flux.readLine();
        }
        flux.close(); 
        return data;
    }
    
    /**
     * Método de lectura de un archivo.
     * @param ruta
     * @return Lectura del archivo en una sola línea.
     * @throws IOException 
     */
    public String leerUnaLinea (String ruta) throws IOException {
        BufferedReader flux = new BufferedReader(new FileReader(ruta));
        String read = flux.readLine();
        String line = "";
        while(read != null){
            line += read + " ";
            read = flux.readLine();
        }
        flux.close();
        return line;
    }
    
    /**
     * Método para escritura de un archivo de texto.
     * @param datos
     * @param ruta
     * @throws IOException 
     */
     public void escribeArchivo(String[] datos, String ruta) throws IOException{
        BufferedWriter salida = new BufferedWriter(new FileWriter(ruta));
        for(int i = 0; i < datos.length; i++){
            salida.write(datos[i]);
            salida.newLine();
        }
        salida.close();
    }
}