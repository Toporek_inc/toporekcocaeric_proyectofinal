package icc.util;

/**
 * 
 * Proyecto Final
 * ICC 2019-1
 * Prof. Virgina Teodosio
 * Ayudantes: Isaac Pompa, Leonardo Gallo
 * Excepción de Búsqueda
 * @title SearchException
 * @author Eric Toporek
 * @version 1.0
 * @date 12/12/18
 * 
 */
public class SearchException extends Exception {

    public SearchException(String string) {
        super(string);
    }
    
}
